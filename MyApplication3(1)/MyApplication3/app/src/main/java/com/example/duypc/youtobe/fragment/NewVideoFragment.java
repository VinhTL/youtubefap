package com.example.duypc.youtobe.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.duypc.youtobe.MainActivity;
import com.example.duypc.youtobe.R;
import com.example.duypc.youtobe.VideoYoutobe;
import com.example.duypc.youtobe.adapter.NewVideoAdapter;
import com.example.duypc.youtobe.cache.SharedpreferenceFavorite;
import com.example.duypc.youtobe.library.CustomRequest;
import com.example.duypc.youtobe.object.NewVideo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by DuyPC on 6/3/2016.
 */
public class NewVideoFragment extends Fragment {
    private ArrayList<NewVideo> listNewVideo = new ArrayList<>();
    private NewVideoAdapter adapterNewVideo;
    private BroadcastReceiver broadcastReceiver;

    private ListView list_Newvideo;
    private TextView tv_number;
    private ProgressBar pbHeaderProgressListview;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.newvideo_fragment,container,false);

        list_Newvideo = (ListView) v.findViewById(R.id.list_Newvideo);
        tv_number=(TextView)v.findViewById(R.id.tv_number);
        pbHeaderProgressListview=(ProgressBar)v.findViewById(R.id.pbHeaderProgressListview);

        parseJson();
        setBroadcastReceiver();
        list_Newvideo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), VideoYoutobe.class);
                intent.putExtra(VideoYoutobe.VIDEO_ID, listNewVideo.get(position).getVideoId());
                intent.putExtra(VideoYoutobe.TITLE, listNewVideo.get(position).getTitle());
                intent.putExtra(VideoYoutobe.VIEWCOUNT, listNewVideo.get(position).getViewCount());
                intent.putExtra(VideoYoutobe.URL, listNewVideo.get(position).getUrl());
                intent.putExtra(VideoYoutobe.PLAYLISTID, listNewVideo.get(position).getPlaylistId());
                intent.putExtra(VideoYoutobe.ACTION, "1");
                getActivity().startActivity(intent);
            }
        });
        return v;
    }
    private void setBroadcastReceiver(){
        // khoi tao brodadcast
        broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                pbHeaderProgressListview.setVisibility(View.GONE);
                switch (intent.getIntExtra("code",1)){
                    case 0:
                        // lay du lieu moi;
                        tv_number.setVisibility(View.GONE);
                        parseJson();
                        break;
                    case 1:
                        // lay du lieu trong favorite;
                        SharedpreferenceFavorite sharedpreferenceFavorite=new SharedpreferenceFavorite();
                        listNewVideo= (ArrayList<NewVideo>) sharedpreferenceFavorite.getHistoryMusic(getActivity());
                        if(listNewVideo==null){
                            // neu khong co du lieu trong danh sach
                            tv_number.setVisibility(View.VISIBLE);
                            listNewVideo=new ArrayList<>();
                            adapterNewVideo = new NewVideoAdapter(listNewVideo,getActivity());
                            list_Newvideo.setAdapter(adapterNewVideo);
                        }else{
                            // co du lieu trong danh sach;
                            tv_number.setVisibility(View.GONE);
                            adapterNewVideo = new NewVideoAdapter(listNewVideo,getActivity());
                            list_Newvideo.setAdapter(adapterNewVideo);
                        }

                        break;
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter("android.intent.action.MAIN");
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // huy broadcast
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    public void parseJson(){
        pbHeaderProgressListview.setVisibility(View.VISIBLE);
        listNewVideo=new ArrayList<>();
        String url = "https://www.googleapis.com/youtube/v3/search?key=AIzaSyCt0QdTz8JJunSgh3uqbiKB71EdAwITXLU&channelId=UC0jDoh3tVXCaqJ6oTve8ebA&part=id%2Csnippet&order=date&maxResults=20";
        final Activity activity=getActivity();
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                adapterNewVideo = new NewVideoAdapter(listNewVideo,getActivity());
                list_Newvideo.setAdapter(adapterNewVideo);
                try {
                    JSONArray jsonArray = response.getJSONArray("items");

                    for(int i=0;i<jsonArray.length();i++){
                        getNewVideo(jsonArray.getJSONObject(i),activity,i,jsonArray.length()-1);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", "tag that bai " + error.getMessage());
                pbHeaderProgressListview.setVisibility(View.GONE);
            }
        });
        jsObjRequest.setShouldCache(false);
        requestQueue.add(jsObjRequest);
    }
    private void getNewVideo(JSONObject jsonObject1, final Activity activity, final int t, final int max) throws JSONException {
        final JSONObject jsonObject2 = jsonObject1.getJSONObject("snippet");
        final JSONObject jsonObject3 = jsonObject1.getJSONObject("id");
        final NewVideo i = new NewVideo();
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        String url_video = "https://www.googleapis.com/youtube/v3/videos?id="+ jsonObject3.getString("videoId")+"&key=AIzaSyCt0QdTz8JJunSgh3uqbiKB71EdAwITXLU&part=snippet,contentDetails,statistics,status";
        i.url = jsonObject2.getJSONObject("thumbnails").getJSONObject("high").getString("url");
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.GET, url_video, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("videos", response.toString());
                    i.title = jsonObject2.getString("title");
                    i.channelTitle = jsonObject2.getString("channelTitle");
                    i.duration = NewVideo.convertDuration(response.getJSONArray("items").getJSONObject(0).getJSONObject("contentDetails").getString("duration"));
                    i.viewCount = NewVideo.convertViewCount(response.getJSONArray("items").getJSONObject(0).getJSONObject("statistics").getString("viewCount").toString());
                    i.setVideoId(jsonObject3.getString("videoId"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                listNewVideo.add(i);
                if(t==max){
                    Intent intent = new Intent("android.intent.action.NEWVIDEOS");
                    intent.putExtra("newviews", listNewVideo);
                    activity.sendBroadcast(intent);
                }
                adapterNewVideo.notifyDataSetChanged();
                if(pbHeaderProgressListview.getVisibility()==View.VISIBLE){
                    pbHeaderProgressListview.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", "tag that bai " + error.getMessage());
                if(pbHeaderProgressListview.getVisibility()==View.VISIBLE){
                    pbHeaderProgressListview.setVisibility(View.GONE);
                }
            }
        });
        jsObjRequest.setShouldCache(false);
        requestQueue.add(jsObjRequest);
    }
}
