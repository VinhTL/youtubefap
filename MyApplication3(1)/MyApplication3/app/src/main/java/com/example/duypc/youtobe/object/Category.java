package com.example.duypc.youtobe.object;

/**
 * Created by DuyPC on 6/7/2016.
 */
public class Category {
    public String url;
    private String title;

    public Category(String url, String title) {
        this.url = url;
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
