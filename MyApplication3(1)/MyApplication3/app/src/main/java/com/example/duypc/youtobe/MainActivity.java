package com.example.duypc.youtobe;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.duypc.youtobe.cache.SharedpreferenceFavorite;
import com.example.duypc.youtobe.fragment.CategoryFragment;
import com.example.duypc.youtobe.fragment.NewVideoFragment;
import com.example.duypc.youtobe.object.Account;
import com.example.duypc.youtobe.object.NewVideo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ActionBarActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private DrawerLayout drawerLayout;
    private ImageView iv_btn_menu, iv_btn_back, iv_btn_search;
    private TextView tv_login,tv_title;
    private LinearLayout ll_login,ll_favorite,ll_home,ll_rate,ll_about;
    private ProgressDialog progress;
    private ViewPager viewPager;
    private SmartTabLayout viewPagerTab;

    private boolean mIntentInProgress;
    private boolean signedInUser;
    private boolean islogin;
    private ConnectionResult mConnectionResult;
    private static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;
    private Account account;
    private ArrayList<NewVideo>newVideos;

    public boolean check = true;
    private BroadcastReceiver broadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        iv_btn_menu = (ImageView) findViewById(R.id.iv_btn_menu);
        iv_btn_back = (ImageView) findViewById(R.id.iv_btn_back);
        iv_btn_search=(ImageView)findViewById(R.id.iv_btn_search);
        tv_login = (TextView) findViewById(R.id.tv_login);
        tv_title=(TextView)findViewById(R.id.tv_title);
        ll_login = (LinearLayout) findViewById(R.id.ll_login);
        ll_favorite = (LinearLayout) findViewById(R.id.ll_favorite);
        ll_home = (LinearLayout) findViewById(R.id.ll_home);
        ll_rate=(LinearLayout)findViewById(R.id.ll_rate);
        ll_about=(LinearLayout)findViewById(R.id.ll_about);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPagerTab = (SmartTabLayout) findViewById(R.id.tablayout);
        iv_btn_search.setVisibility(View.GONE);
        setViewPager();
        setDrawerLayout();
        setmGoogleApiClient();
        setBroadcastReceiver();

        iv_btn_menu.setOnClickListener(this);
        iv_btn_back.setOnClickListener(this);
        ll_login.setOnClickListener(this);
        ll_favorite.setOnClickListener(this);
        ll_home.setOnClickListener(this);
        ll_favorite.setOnClickListener(this);
        ll_rate.setOnClickListener(this);
        ll_about.setOnClickListener(this);
        iv_btn_search.setOnClickListener(this);
    }
    private void setBroadcastReceiver(){
        broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                newVideos=new ArrayList<>();
                newVideos= (ArrayList<NewVideo>) intent.getSerializableExtra("newviews");
                String [] strings=new String[newVideos.size()];
                for(int i=0;i<newVideos.size();i++){
                    strings[i]=newVideos.get(i).getTitle();
                }
                iv_btn_search.setVisibility(View.VISIBLE);
            }
        };
        IntentFilter intentFilter = new IntentFilter("android.intent.action.NEWVIDEOS");
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // huy broadcast
        unregisterReceiver(broadcastReceiver);
    }
    //    set viewpager
    private void setViewPager() {
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add("NEW VIDEOS", NewVideoFragment.class)
                .add("CATEGORY", CategoryFragment.class)
                .create());
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);
        final LayoutInflater inflater = LayoutInflater.from(this);
        viewPagerTab.setCustomTabView(new SmartTabLayout.TabProvider() {
            @Override
            public View createTabView(ViewGroup container, int position, PagerAdapter adapter) {
                View itemView = inflater.inflate(R.layout.custom_tab_text, container, false);
                TextView text = (TextView) itemView.findViewById(R.id.custom_tab_text);
                text.setText(adapter.getPageTitle(position));
                return itemView;
            }
        });
        viewPagerTab.setViewPager(viewPager);

    }

    private void setmGoogleApiClient(){
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();
    }
    private void setDrawerLayout(){
        account=getAccount();
        if(account.getId().length()==0){
            islogin=false;
            tv_login.setText("Login");
        }else{
            islogin=true;
            tv_login.setText("Logout");
        }
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                R.drawable.ic_favorite,
                R.string.accept,
                R.string.accept
        ) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (slideOffset == 0) {
                    iv_btn_menu.setVisibility(View.VISIBLE);
                    iv_btn_back.setVisibility(View.GONE);
                    invalidateOptionsMenu();
                } else if (slideOffset != 0) {
                    iv_btn_menu.setVisibility(View.GONE);
                    iv_btn_back.setVisibility(View.VISIBLE);
                    invalidateOptionsMenu();
                }
                super.onDrawerSlide(drawerView, slideOffset);
            }
        };
        drawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_btn_menu:
                drawerLayout.openDrawer(Gravity.LEFT);
                break;
            case R.id.iv_btn_back:
                drawerLayout.closeDrawers();
                break;
            case R.id.ll_login:
                drawerLayout.closeDrawers();
                progress = ProgressDialog.show(this, "",
                        "Đợi trọng giây nát", true);
                if (islogin) {
                    // logout
                    googlePlusLogout();
                } else {
                    // login
                    googlePlusLogin();
                }
                break;
            case R.id.ll_home:
                tv_title.setText("Home");
                drawerLayout.closeDrawers();
                Intent intent = new Intent("android.intent.action.MAIN");
                intent.putExtra("code", 0);
                sendBroadcast(intent);
                viewPager.setCurrentItem(0);
                break;
            case R.id.ll_favorite:
                tv_title.setText("Favorite");
                drawerLayout.closeDrawers();
                intent = new Intent("android.intent.action.MAIN");
                intent.putExtra("code", 1);
                sendBroadcast(intent);
                viewPager.setCurrentItem(0);

                break;
            case R.id.iv_btn_search:
                drawerLayout.closeDrawers();
                intent = new Intent(this, SearchActivity.class);
                intent.putExtra("newvideos",newVideos);
                startActivity(intent);
                break;
            case R.id.ll_rate:
                drawerLayout.closeDrawers();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.snapsofts.football"));
                startActivity(browserIntent);
                break;
            case R.id.ll_about:
                drawerLayout.closeDrawers();
                intent = new Intent(this, AboutAcivity.class);
                startActivity(intent);
                break;
        }
    }

    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
            return;
        }
        if (!mIntentInProgress) {
            mConnectionResult = result;
            if (signedInUser) {
                resolveSignInError();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        switch (requestCode) {
            case RC_SIGN_IN:
                if (responseCode == RESULT_OK) {
                    signedInUser = false;
                }
                mIntentInProgress = false;
                if (!mGoogleApiClient.isConnecting()) {
                    mGoogleApiClient.connect();
                }
                break;
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        signedInUser = false;
        getProfileInformation();
    }

    private void updateProfile(boolean isSignedIn) {
        if(progress!=null)progress.dismiss();
        if (isSignedIn) {
            tv_login.setText("Logout");
            islogin = true;
        } else {
            tv_login.setText("Login");
            islogin = false;
            deleteAccount();
        }

    }

    private void getProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                String code=currentPerson.getId();
                String personName = currentPerson.getDisplayName();
                String personPhotoUrl = currentPerson.getImage().getUrl();
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                Account account=new Account(code,email,personName,personPhotoUrl);
                saveAccount(account);
                updateProfile(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
        updateProfile(false);
    }

    private void googlePlusLogin() {
        if (!mGoogleApiClient.isConnecting()) {
            signedInUser = true;
            resolveSignInError();
        }
    }

    private void googlePlusLogout() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
            updateProfile(false);
        }
    }
    private void saveAccount(Account account){
        SharedPreferences sharedPreferences = getSharedPreferences("account", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("code", account.getId());
        editor.putString("name", account.getName());
        editor.putString("gmail", account.getGmail());
        editor.putString("url", account.getUrl());
        editor.commit();
    }
    private void deleteAccount(){
        SharedPreferences sharedPreferences = getSharedPreferences("account", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("code","");
        editor.putString("name","");
        editor.putString("gmail","");
        editor.putString("url","");
        editor.commit();
    }
    private Account getAccount(){
        SharedPreferences sharedPreferences = getSharedPreferences("account", MODE_PRIVATE);
        return new Account(sharedPreferences.getString("code", "")+"",
                sharedPreferences.getString("gmail", "")+"",
                sharedPreferences.getString("name", "")+"",
                sharedPreferences.getString("url", "")+"");
    }
}
