package com.example.duypc.youtobe.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.duypc.youtobe.R;
import com.example.duypc.youtobe.adapter.Category_adapter;
import com.example.duypc.youtobe.adapter.NewVideoAdapter;
import com.example.duypc.youtobe.fragment.dulieu.Category_dulieu;
import com.example.duypc.youtobe.library.CustomRequest;
import com.example.duypc.youtobe.object.Category;
import com.example.duypc.youtobe.object.NewVideo;
import com.example.duypc.youtobe.object.Playlist;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by DuyPC on 6/3/2016.
 */
public class CategoryFragment extends Fragment{
    private ListView list_Newvideo;
    private TextView tv_number;
    private ProgressBar pbHeaderProgressListview;

    private Category_adapter category_adapter;
    private ArrayList<Playlist> list_category;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.newvideo_fragment,container,false);
        list_Newvideo = (ListView) v.findViewById(R.id.list_Newvideo);
        tv_number=(TextView)v.findViewById(R.id.tv_number);
        pbHeaderProgressListview=(ProgressBar)v.findViewById(R.id.pbHeaderProgressListview);
        parseJson();
        return v;
    }
    public void parseJson(){
        list_category=new ArrayList<>();
        pbHeaderProgressListview.setVisibility(View.VISIBLE);
        String url = "https://www.googleapis.com/youtube/v3/playlists?part=snippet%2CcontentDetails&channelId=UC0jDoh3tVXCaqJ6oTve8ebA&key=AIzaSyCt0QdTz8JJunSgh3uqbiKB71EdAwITXLU";
        final Activity activity=getActivity();
        final RequestQueue requestQueue = Volley.newRequestQueue(activity);
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                category_adapter = new Category_adapter(list_category,activity);
                list_Newvideo.setAdapter(category_adapter);
                try {
                    JSONArray jsonArray = response.getJSONArray("items");
                    for(int i=0;i<jsonArray.length();i++){
                        getNewVideo(jsonArray.getJSONObject(i),activity);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("sdfdgf", "get  " + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", "tag that bai " + error.getMessage());
                pbHeaderProgressListview.setVisibility(View.GONE);
            }
        });
        jsObjRequest.setShouldCache(false);
        requestQueue.add(jsObjRequest);
    }
    private void getNewVideo(final JSONObject jsonObject1, final Activity activity) throws JSONException {
        final JSONObject jsonObject0 = jsonObject1.getJSONObject("contentDetails");
        if(!jsonObject0.getString("itemCount").equals("0")){
            final JSONObject jsonObject2 = jsonObject1.getJSONObject("snippet");
            final Playlist i = new Playlist();
            String url = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails%2Cstatus&playlistId="+ jsonObject1.getString("id") +"&key=AIzaSyCt0QdTz8JJunSgh3uqbiKB71EdAwITXLU";
            final RequestQueue requestQueue = Volley.newRequestQueue(activity);
            CustomRequest jsObjRequest = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    try {
                        i.title = jsonObject2.getString("title");
                        i.id = jsonObject1.getString("id");
                        JSONArray jsonArray = response.getJSONArray("items");
                        JSONObject jsonObject11 = jsonArray.getJSONObject(jsonArray.length()-1);
                        JSONObject jsonObject22 = jsonObject11.getJSONObject("snippet");
                        i.thumbUrl = jsonObject22.getJSONObject("thumbnails").getJSONObject("high").getString("url");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("đâsd", "get  " + e.getMessage());
                    }
                    list_category.add(i);
                    category_adapter.notifyDataSetChanged();
                    if(pbHeaderProgressListview.getVisibility()==View.VISIBLE){
                        pbHeaderProgressListview.setVisibility(View.GONE);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if(pbHeaderProgressListview.getVisibility()==View.VISIBLE){
                        pbHeaderProgressListview.setVisibility(View.GONE);
                    }
                    Log.e("error", "tag that bai " + error.getMessage());
                }

            });
            jsObjRequest.setShouldCache(false);
            requestQueue.add(jsObjRequest);
        }
    }
}
