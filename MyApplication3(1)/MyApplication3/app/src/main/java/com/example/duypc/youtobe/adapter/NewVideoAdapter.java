package com.example.duypc.youtobe.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.duypc.youtobe.R;
import com.example.duypc.youtobe.VideoYoutobe;
import com.example.duypc.youtobe.object.NewVideo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DuyPC on 6/3/2016.
 */
public class NewVideoAdapter extends BaseAdapter {
    private List<NewVideo> listNewVideo;
    private Activity activity;

    public NewVideoAdapter(ArrayList<NewVideo> listNewVideo, Activity activity) {
        this.listNewVideo = listNewVideo;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return listNewVideo.size();
    }

    @Override
    public Object getItem(int position) {
        return listNewVideo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v == null){
            v = LayoutInflater.from(activity).inflate(R.layout.item_newvideo, null);
        }
        NewVideo item =  listNewVideo.get(position);
        ImageView iv_url = (ImageView) v.findViewById(R.id.iv_url);
        TextView tv_title = (TextView) v.findViewById(R.id.tv_title);
        TextView tv_duration = (TextView) v.findViewById(R.id.tv_duration);
        TextView tv_viewCount = (TextView) v.findViewById(R.id.tv_viewCount);
        String url = item.getUrl();
        Picasso.with(activity)
                .load(url)
                .resize(250,250)
                .centerCrop()
                .into(iv_url);

        tv_title.setText(item.getTitle());
        tv_duration.setText(item.getDuration());
        tv_viewCount.setText(item.getViewCount()+" Views");
        return v;
    }
}
