package com.example.duypc.youtobe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.duypc.youtobe.adapter.NewVideoAdapter;
import com.example.duypc.youtobe.object.NewVideo;

import java.util.ArrayList;

/**
 * Created by Admin on 06/08/16.
 */
public class SearchActivity extends Activity {
    private EditText et_search;
    private ImageView iv_btn_back;
    private ListView lv_playlist_category;
    private ArrayList<NewVideo>newVideos,list;
    private NewVideoAdapter newVideoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        et_search=(EditText)findViewById(R.id.et_search);
        iv_btn_back=(ImageView)findViewById(R.id.iv_btn_back);
        lv_playlist_category=(ListView)findViewById(R.id.lv_playlist_category);
        iv_btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        newVideos= (ArrayList<NewVideo>) getIntent().getSerializableExtra("newvideos");
        list=newVideos;
        newVideoAdapter = new NewVideoAdapter(list,this);
        lv_playlist_category .setAdapter(newVideoAdapter);
        et_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                list = new ArrayList<>();
                if (et_search.getText().toString().length() == 0) {
                    list = newVideos;
                } else {
                    for (int i = 0; i < newVideos.size(); i++) {
                        if (newVideos.get(i).getTitle().toLowerCase().indexOf(et_search.getText().toString().toLowerCase())>=0){
                            list.add(newVideos.get(i));
                        }
                    }
                }
                newVideoAdapter = new NewVideoAdapter(list, SearchActivity.this);
                lv_playlist_category.setAdapter(newVideoAdapter);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        lv_playlist_category.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(SearchActivity.this, VideoYoutobe.class);
                intent.putExtra(VideoYoutobe.VIDEO_ID, newVideos.get(position).getVideoId());
                intent.putExtra(VideoYoutobe.TITLE, newVideos.get(position).getTitle());
                intent.putExtra(VideoYoutobe.VIEWCOUNT, newVideos.get(position).getViewCount());
                intent.putExtra(VideoYoutobe.URL, newVideos.get(position).getUrl());
                intent.putExtra(VideoYoutobe.PLAYLISTID, newVideos.get(position).getPlaylistId());
                intent.putExtra(VideoYoutobe.ACTION, "1");
                startActivity(intent);
            }
        });
    }
}
