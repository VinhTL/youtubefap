package com.example.duypc.youtobe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.duypc.youtobe.PlayerYoutobeCode.PlayerActivity;
import com.example.duypc.youtobe.adapter.NewVideoAdapter;
import com.example.duypc.youtobe.cache.SharedpreferenceFavorite;
import com.example.duypc.youtobe.config.Key;
import com.example.duypc.youtobe.library.CustomRequest;
import com.example.duypc.youtobe.object.NewVideo;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by DuyPC on 6/6/2016.
 */
public class VideoYoutobe extends PlayerActivity implements View.OnClickListener {
    private YouTubePlayerView playerView;
    public static final String VIDEO_ID = "videoId";
    public static final String TITLE = "title";
    public static final String VIEWCOUNT = "viewCount";
    public static final String URL = "url";
    public static final String PLAYLISTID = "playlistId";
    public static final String NEWVIDEOS = "newvideos";
    public static final String ACTION = "action";

    public String videoId = "", title = "", viewCount = "", url = "", playlistId = "",action;

    private TextView tv_title, tv_luotxem;
    private LinearLayout ll_btn_favorite;
    public SharedpreferenceFavorite saveFavorite;
    private ArrayList<NewVideo> newVideos;
    private ProgressBar pbHeaderProgressListview;
    private NewVideoAdapter newVideoAdapter;
    private ListView lv_lienquan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videoyoutobe);
        saveFavorite = new SharedpreferenceFavorite();

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_luotxem = (TextView) findViewById(R.id.tv_luotxem);
        ll_btn_favorite = (LinearLayout) findViewById(R.id.ll_btn_favorite);
        pbHeaderProgressListview = (ProgressBar) findViewById(R.id.pbHeaderProgressListview);
        lv_lienquan = (ListView) findViewById(R.id.lv_lienquan);

        videoId = getIntent().getStringExtra(VIDEO_ID);
        title = getIntent().getStringExtra(TITLE);
        viewCount = getIntent().getStringExtra(VIEWCOUNT);
        url = getIntent().getStringExtra(URL);
        playlistId = getIntent().getStringExtra(PLAYLISTID);
        action = getIntent().getStringExtra(ACTION);

        tv_title.setText(title);
        tv_luotxem.setText(viewCount + " lượt xem");
        playerView = (YouTubePlayerView) findViewById(R.id.player);

        if(action.equals("0")){
            newVideos= (ArrayList<NewVideo>) getIntent().getSerializableExtra(NEWVIDEOS);
            newVideoAdapter = new NewVideoAdapter(newVideos, VideoYoutobe.this);
            lv_lienquan.setAdapter(newVideoAdapter);
        }else{
            if (playlistId.length() != 0) {
                String url = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails%2Cstatus&playlistId=" + playlistId + "&key=AIzaSyCt0QdTz8JJunSgh3uqbiKB71EdAwITXLU";
                setNewVideos(url, 0);
            } else {
                String url = "https://www.googleapis.com/youtube/v3/search?key=AIzaSyCt0QdTz8JJunSgh3uqbiKB71EdAwITXLU&channelId=UC0jDoh3tVXCaqJ6oTve8ebA&part=id%2Csnippet&order=date&maxResults=20";
                setNewVideos(url, 1);
            }
        }

        try {
            playerView.initialize(Key.DEVELOPER_KEY, this);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        ll_btn_favorite.setOnClickListener(this);

        lv_lienquan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(VideoYoutobe.this, VideoYoutobe.class);
                intent.putExtra(VideoYoutobe.VIDEO_ID, newVideos.get(position).getVideoId());
                intent.putExtra(VideoYoutobe.TITLE, newVideos.get(position).getTitle());
                intent.putExtra(VideoYoutobe.VIEWCOUNT, newVideos.get(position).getViewCount());
                intent.putExtra(VideoYoutobe.URL, newVideos.get(position).getUrl());
                intent.putExtra(VideoYoutobe.PLAYLISTID, newVideos.get(position).getPlaylistId());
                intent.putExtra(VideoYoutobe.NEWVIDEOS,newVideos);
                intent.putExtra(VideoYoutobe.ACTION,"0");
                VideoYoutobe.this.startActivity(intent);
                overridePendingTransition(0,0);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        VideoYoutobe.this.finish();
                    }
                },100);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_btn_favorite:
                NewVideo newVideo = new NewVideo(title, viewCount, url, videoId);
                saveFavorite.addHistoryMusic(this, newVideo);
                break;
        }
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.player);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {
            youTubePlayer.cueVideo(videoId);
        }

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        super.onInitializationFailure(provider, errorReason);
    }

    private void setNewVideos(String url, final int a) {
        pbHeaderProgressListview.setVisibility(View.VISIBLE);
        newVideos = new ArrayList<>();
        final Activity activity = this;
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                newVideoAdapter = new NewVideoAdapter(newVideos, activity);
                lv_lienquan.setAdapter(newVideoAdapter);
                try {
                    JSONArray jsonArray = response.getJSONArray("items");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        getNewVideo(jsonArray.getJSONObject(i),a, activity);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", "tag that bai " + error.getMessage());
                pbHeaderProgressListview.setVisibility(View.GONE);
            }
        });
        jsObjRequest.setShouldCache(false);
        requestQueue.add(jsObjRequest);
    }

    private void getNewVideo(final JSONObject jsonObject1, final int a, final Activity activity) throws JSONException {
        final JSONObject jsonObject2 = jsonObject1.getJSONObject("snippet");
        final JSONObject jsonObject3;
        if (a == 0) {
            jsonObject3 = jsonObject2.getJSONObject("resourceId");
        } else {
            jsonObject3 = jsonObject1.getJSONObject("id");
        }
        final NewVideo i = new NewVideo();
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        String url_video = "https://www.googleapis.com/youtube/v3/videos?id=" + jsonObject3.getString("videoId") + "&key=AIzaSyCt0QdTz8JJunSgh3uqbiKB71EdAwITXLU&part=snippet,contentDetails,statistics,status";
        i.url = jsonObject2.getJSONObject("thumbnails").getJSONObject("high").getString("url");
        CustomRequest jsObjRequest = new CustomRequest(Request.Method.GET, url_video, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("videos", response.toString());
                    i.title = jsonObject2.getString("title");
                    if (a == 0) {
                        i.playlistId = jsonObject2.getString("playlistId");
                    }
                    i.channelTitle = jsonObject2.getString("channelTitle");
                    i.duration = NewVideo.convertDuration(response.getJSONArray("items").getJSONObject(0).getJSONObject("contentDetails").getString("duration"));
                    i.viewCount = NewVideo.convertViewCount(response.getJSONArray("items").getJSONObject(0).getJSONObject("statistics").getString("viewCount").toString());
                    i.setVideoId(jsonObject3.getString("videoId"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                newVideos.add(i);
                newVideoAdapter.notifyDataSetChanged();
                if (pbHeaderProgressListview.getVisibility() == View.VISIBLE) {
                    pbHeaderProgressListview.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pbHeaderProgressListview.getVisibility() == View.VISIBLE) {
                    pbHeaderProgressListview.setVisibility(View.GONE);
                }
                Log.e("error", "tag that bai " + error.getMessage());
            }
        });
        jsObjRequest.setShouldCache(false);
        requestQueue.add(jsObjRequest);
    }
}
