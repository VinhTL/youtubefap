package com.example.duypc.youtobe.cache;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;


import com.example.duypc.youtobe.object.NewVideo;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by DuyPC on 4/26/2016.
 */
public class SharedpreferenceFavorite {
    public static final String PREFS_NAME = "HISTORY";
    public static final String HISTORY = "History_Songs";
    public SharedpreferenceFavorite(){
        super();
    }
    public void saveHistoryMusic(Context context,List<NewVideo> listHistorySongs){
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        Gson gson = new Gson();
        String jsonHistory = gson.toJson(listHistorySongs);
        editor.putString(HISTORY, jsonHistory);
        editor.commit();
    }
    public void addHistoryMusic(Context context, NewVideo songItem){
        List<NewVideo> listHistorySongs = getHistoryMusic(context);
        if(listHistorySongs == null){
            listHistorySongs = new ArrayList<>();
        }
        listHistorySongs.add(songItem);
        saveHistoryMusic(context, listHistorySongs);
    }
    // xóa từng thằng 1
    public void removeHistoryMusic(Context context, int position){
        Log.d("SongItem",position+"");
        List<NewVideo> listHistorySongs = getHistoryMusic(context);
        if(listHistorySongs != null){
            listHistorySongs.remove(position);
            saveHistoryMusic(context, listHistorySongs);
        }
    }
    // xóa tất cả
    public void removeAllHistoryMusic(Context context){
        List<NewVideo> listHistorySongs = getHistoryMusic(context);
        if(listHistorySongs != null){
            listHistorySongs = new ArrayList<NewVideo>();
            saveHistoryMusic(context, listHistorySongs);
        }
    }
    public List<NewVideo> getHistoryMusic(Context context){
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        List<NewVideo> listHistorySongs;
        if(settings.contains(HISTORY)){
            String jsonHistory = settings.getString(HISTORY, null);
            Gson gson = new Gson();
            NewVideo[] songItems = gson.fromJson(jsonHistory, NewVideo[].class);
            listHistorySongs = Arrays.asList(songItems);
            listHistorySongs = new ArrayList<NewVideo>(listHistorySongs);
        } else {
            return null;
        }
        return listHistorySongs;
    }
}
